%define gstreamername gstreamer1
%global __python %{__python3}

Name: gstlal-inspiral
Version: 1.10.0
Release: 1.1%{?dist}
Summary: GSTLAL Experimental Supplements
License: GPL
Group: LSC Software/Data Analysis

# --- package requirements --- #
Requires: gstlal >= 1.10.0
Requires: gstlal-ugly >= 1.10.0
Requires: gobject-introspection >= 1.30.0
Requires: %{gstreamername} >= 1.14.1
Requires: %{gstreamername}-plugins-base >= 1.14.1
Requires: %{gstreamername}-plugins-good >= 1.14.1
Requires: %{gstreamername}-plugins-bad-free
Requires: gsl

# --- LSCSoft package requirements --- #
Requires: lal >= 7.2.4
Requires: lalmetaio >= 3.0.2
Requires: lalinspiral >= 3.0.2
Requires: ligo-gracedb >= 2.7.5
Requires: python3 >= 3.6
Requires: python%{python3_pkgversion}-%{gstreamername}
Requires: python%{python3_pkgversion}-gwdatafind
Requires: python%{python3_pkgversion}-lal >= 7.2.4
Requires: python%{python3_pkgversion}-lalinspiral >= 3.0.2
Requires: python%{python3_pkgversion}-ligo-lw >= 1.8.3
Requires: python%{python3_pkgversion}-ligo-lw-bin >= 1.8.3
Requires: python%{python3_pkgversion}-ligo-segments >= 1.2.0
Requires: python%{python3_pkgversion}-ligo-scald >= 0.7.2

# --- python package requirements --- #
Requires: python%{python3_pkgversion}-tqdm
Requires: python%{python3_pkgversion}-numpy >= 1.7.0
Requires: python%{python3_pkgversion}-scipy
Requires: python%{python3_pkgversion}-h5py

%if 0%{?rhel} == 8
Requires: python%{python3_pkgversion}-numpy >= 1.7.0
Requires: python3-matplotlib
%else
Requires: numpy >= 1.7.0
Requires: python%{python3_pkgversion}-matplotlib
%endif

# --- build requirements
BuildRequires: doxygen >= 1.8.3
BuildRequires: gobject-introspection-devel >= 1.30.0
BuildRequires: graphviz
BuildRequires: gstlal-devel >= 1.10.0
BuildRequires: %{gstreamername}-devel >= 1.14.1
BuildRequires: %{gstreamername}-plugins-base-devel >= 1.14.1
BuildRequires: gsl-devel
BuildRequires: gtk-doc >= 1.11
BuildRequires: liblal-devel >= 7.2.4
BuildRequires: liblalmetaio-devel >= 3.0.2
BuildRequires: liblalinspiral-devel >= 3.0.2
BuildRequires: pkgconfig >= 0.18.0
BuildRequires: python3-devel >= 3.6
BuildRequires: python%{python3_pkgversion}-lal >= 7.2.4
BuildRequires: python%{python3_pkgversion}-lalinspiral >= 3.0.2
Source: https://software.igwn.org/lscsoft/source/gstlal-inspiral-%{version}.tar.gz
URL: https://git.ligo.org/lscsoft/gstlal
Packager: Kipp Cannon <kipp.cannon@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package provides a variety of gstreamer elements for
gravitational-wave data analysis and some libraries to help write such
elements.  The code here sits on top of several other libraries, notably
the LIGO Algorithm Library (LAL), FFTW, the GNU Scientific Library (GSL),
and, of course, GStreamer.

This package contains plugins, libraries, and programs for inspiral data
analysis.


%package devel
Summary: Files and documentation needed for compiling gstlal-inspiral based plugins and programs.
Group: LSC Software/Data Analysis
Requires: gstlal-devel >= 1.10.0 
Requires: python3-devel >= 3.6 
Requires: %{gstreamername}-devel >= 1.14.1 
Requires: %{gstreamername}-plugins-base-devel >= 1.14.1 
Requires: liblal-devel >= 7.2.4 
Requires: liblalmetaio-devel >= 3.0.2 
Requires: liblalinspiral-devel >= 3.0.2 
Requires: gsl-devel
%description devel
This package contains the files needed for building gstlal-inspiral based
plugins and programs.


%prep
%setup -q -n %{name}-%{version}


%build
%configure --enable-gtk-doc --disable-massmodel PYTHON=python3
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete


%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_datadir}/gstlal/*
%{_docdir}/gstlal-inspiral-*
%{_libdir}/*.so.*
%{_libdir}/gstreamer-1.0/*.so
#%{_libdir}/gstreamer-1.0/python/*
%{_prefix}/%{_lib}/python*/site-packages/gstlal

%files devel
%defattr(-,root,root)
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/gstreamer-1.0/*.a
%{_libdir}/pkgconfig/*
%{_includedir}/*
